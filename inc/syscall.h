#ifndef SYSCALL
#define SYSCALL

#ifdef __linux__

#if (defined __amd64__  || defined __amd64 || defined __x86_64__ || defined __x86_64 )

#include "syscalls/x86_64/syscalls_linux.h"

#elif

#error "Architecture is not supported at the moment"

#endif /*(__amd64__) || (__amd64) || (__x86_64__) || (__x86_64)*/

#elif __FreeBSD__

#if (defined __amd64__  || defined __amd64 || defined __x86_64__ || defined __x86_64 )

#include "syscalls/x86_64/syscalls_bsd.h"

#elif

#error "Architecture is not supported at the moment"

#endif /*(__amd64__) || (__amd64) || (__x86_64__) || (__x86_64)*/

#else

#error "OS is not supported at the moment"

#endif /* __linux__ */

#endif /* SYSCALL */
