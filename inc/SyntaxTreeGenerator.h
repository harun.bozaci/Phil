#ifndef SYNTAX_TREE_GENERATOR_H
#define SYNTAX_TREE_GENERATOR_H
#include <stddef.h>

enum ExpressionType
{
  EXPR_EXIT,
  EXPR_NO
};

typedef enum ExpressionType ExpressionType_t;

typedef struct TokenNode *TokenNodePtr_t;

struct Expression
{
  ExpressionType_t type; 
  TokenNodePtr_t operands;
  size_t noOperands;
};

typedef struct Expression Expression_t;

typedef struct Parser Parser_t;

typedef Parser_t *ParserPtr_t;

struct Parser
{
  Expression_t expression;
  ParserPtr_t next;
};

typedef struct Arena Arena_t;

ParserPtr_t GenerateParseTree(TokenNodePtr_t token_start, Arena_t *parser_buffer);

void PrintParser(ParserPtr_t node);

#endif // SYNTAX_TREE_GENERATOR_H
