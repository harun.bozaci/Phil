#ifndef UTILITY_H
#define UTILITY_H
#include <stdbool.h>
#include <stddef.h>
#include "SV.h"

bool IsSpace(char ch);
bool IsSpecial(char ch);
bool IsKeyword(SV view);
bool IsSign(char ch);
bool IsAllNumeric(SV view);
bool IsStringLiteral(SV view);
bool IsLiteral(SV view);
bool IsIdentifier(SV view);
bool IsSemiColon(char ch);

#endif // UTILITY_H
