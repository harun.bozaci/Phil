#ifndef ASSEMBLY_GENERATOR_H
#define ASSEMBLY_GENERATOR_H

#include "SV.h"

typedef struct Parser *ParserPtr_t;
typedef struct Arena Arena_t;

SV GenerateAssemblyCode(ParserPtr_t parser, Arena_t* output_buffer);

#endif /* ASSEMBLY_GENERATOR_H */
