#ifndef COMMON_H
#define COMMON_H

#include "SV.h"

#define spaces SVConsSize(" \r\n\t", 4)
#define specials SVConsSize(",;+-/*{}()><=%#!~\\\"'[]@", 23)

#define keywords ((SV[]){ SVConsSize("for", 3) \
                        , SVConsSize("if", 2) \
                        , SVConsSize("return", 6) \
                        , SVConsSize("in", 2) \
                        , SVConsSize("exit", 4) })

#define DECL_PAIR(TYPE1, TYPE2) struct Pair_##TYPE1##_##TYPE2
#define DEF_PAIR(TYPE1, TYPE2) \
  DECL_PAIR(TYPE1,TYPE2) {\
    TYPE1 first; \
    TYPE2 second; \
  }

#endif // COMMON_H
