#ifndef TOKEN_H
#define TOKEN_H

#include "SV.h"
#include "Common.h"

enum TokenType
{
  TOKEN_LITERAL = 0x00,
  TOKEN_IDENTIFIER,
  TOKEN_OPERATOR,
  TOKEN_KEYWORD,
  TOKEN_SEMICOLON,
  TOKEN_NO
};

typedef enum TokenType TokenTypeCategory_t;

enum LiteralTokenType
{
  TOK_LIT_STRING = 0x00,
  TOK_LIT_INT,
  TOK_LIT_NO
};

typedef enum LiteralTokenType LiteralTokenType_t;

enum OperatorTokenType
{
  TOK_OP_EQ = 0x00,
  TOK_OP_PLUS,
  TOK_OP_DASH,
  TOK_OP_STAR,
  TOK_OP_FSLASH,
  TOK_OP_MODULO,
  TOK_OP_NO
};

typedef enum OperatorTokenType OperatorTokenType_t;

enum KeywordTokenType
{
  TOK_KW_FOR = 0x00,
  TOK_KW_IF,
  TOK_KW_RETURN,
  TOK_KW_IN,
  TOK_KW_EXIT,
  TOK_KW_NO
};

typedef enum KeywordTokenType KeywordTokenType_t;

typedef union SubTokenType {
  LiteralTokenType_t subLit;
  OperatorTokenType_t subOp;
  KeywordTokenType_t subKw;
} SubTokenType_t;

typedef struct {
  TokenTypeCategory_t category;
  union { SubTokenType_t stt; size_t idx; } subToken;
} TokenType_t;

struct Token
{
  TokenType_t type;
  SV value;
};

typedef struct Token Token_t;

struct TokenNode
{
  Token_t token;
  struct TokenNode *next;
};

typedef struct TokenNode TokenNode_t;

typedef TokenNode_t *TokenNodePtr_t;

typedef DEF_PAIR(TokenNodePtr_t, TokenNodePtr_t) TokenNodePtrPair;

struct Arena;

typedef struct Arena Arena_t;

TokenNodePtrPair Tokenize(SV input, Arena_t *arena);

void PrintToken(TokenNodePtr_t token);

#endif // TOKEN_H
