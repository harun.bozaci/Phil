#ifndef ARENA_H
#define ARENA_H
#include <stddef.h>

#ifndef MAX_ARENA_CAPACITY
#define MAX_ARENA_CAPACITY 500 * 1024
#endif

#ifndef MAX_ARENA_SIZE
#define MAX_ARENA_SIZE 5
#endif

struct Arena
{
  void* block;
  size_t size;
};

typedef struct Arena Arena_t;

Arena_t* init_arena(void);

void* arena_alloc(Arena_t* arena, size_t size);

void destroy_arena(Arena_t* arena);

#endif // ARENA_H
