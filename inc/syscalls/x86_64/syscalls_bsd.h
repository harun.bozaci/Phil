#ifndef SYSCALLS_BSD
#define SYSCALLS_BSD

#define	SYSCALL_NO	0
#define	EXIT_NO	1
#define	FORK_NO	2
#define	READ_NO	3
#define	WRITE_NO	4
#define	OPEN_NO	5
#define	CLOSE_NO	6
#define	WAIT4_NO	7
				/* 8 is old creat */
#define	LINK_NO	9
#define	UNLINK_NO	10
				/* 11 is obsolete execv */
#define	CHDIR_NO	12
#define	FCHDIR_NO	13
#define	FREEBSD11_MKNOD_NO	14
#define	CHMOD_NO	15
#define	CHOWN_NO	16
#define	BREAK_NO	17
				/* 18 is freebsd4 getfsstat */
				/* 19 is old lseek */
#define	GETPID_NO	20
#define	MOUNT_NO	21
#define	UNMOUNT_NO	22
#define	SETUID_NO	23
#define	GETUID_NO	24
#define	GETEUID_NO	25
#define	PTRACE_NO	26
#define	RECVMSG_NO	27
#define	SENDMSG_NO	28
#define	RECVFROM_NO	29
#define	ACCEPT_NO	30
#define	GETPEERNAME_NO	31
#define	GETSOCKNAME_NO	32
#define	ACCESS_NO	33
#define	CHFLAGS_NO	34
#define	FCHFLAGS_NO	35
#define	SYNC_NO	36
#define	KILL_NO	37
				/* 38 is old stat */
#define	GETPPID_NO	39
				/* 40 is old lstat */
#define	DUP_NO	41
#define	FREEBSD10_PIPE_NO	42
#define	GETEGID_NO	43
#define	PROFIL_NO	44
#define	KTRACE_NO	45
				/* 46 is old sigaction */
#define	GETGID_NO	47
				/* 48 is old sigprocmask */
#define	GETLOGIN_NO	49
#define	SETLOGIN_NO	50
#define	ACCT_NO	51
				/* 52 is old sigpending */
#define	SIGALTSTACK_NO	53
#define	IOCTL_NO	54
#define	REBOOT_NO	55
#define	REVOKE_NO	56
#define	SYMLINK_NO	57
#define	READLINK_NO	58
#define	EXECVE_NO	59
#define	UMASK_NO	60
#define	CHROOT_NO	61
				/* 62 is old fstat */
				/* 63 is old getkerninfo */
				/* 64 is old getpagesize */
#define	MSYNC_NO	65
#define	VFORK_NO	66
				/* 67 is obsolete vread */
				/* 68 is obsolete vwrite */
#define	SBRK_NO	69
#define	SSTK_NO	70
				/* 71 is old mmap */
#define	FREEBSD11_VADVISE_NO	72
#define	MUNMAP_NO	73
#define	MPROTECT_NO	74
#define	MADVISE_NO	75
				/* 76 is obsolete vhangup */
				/* 77 is obsolete vlimit */
#define	MINCORE_NO	78
#define	GETGROUPS_NO	79
#define	SETGROUPS_NO	80
#define	GETPGRP_NO	81
#define	SETPGID_NO	82
#define	SETITIMER_NO	83
				/* 84 is old wait */
#define	SWAPON_NO	85
#define	GETITIMER_NO	86
				/* 87 is old gethostname */
				/* 88 is old sethostname */
#define	GETDTABLESIZE_NO	89
#define	DUP2_NO	90
#define	FCNTL_NO	92
#define	SELECT_NO	93
#define	FSYNC_NO	95
#define	SETPRIORITY_NO	96
#define	SOCKET_NO	97
#define	CONNECT_NO	98
				/* 99 is old accept */
#define	GETPRIORITY_NO	100
				/* 101 is old send */
				/* 102 is old recv */
				/* 103 is old sigreturn */
#define	BIND_NO	104
#define	SETSOCKOPT_NO	105
#define	LISTEN_NO	106
				/* 107 is obsolete vtimes */
				/* 108 is old sigvec */
				/* 109 is old sigblock */
				/* 110 is old sigsetmask */
				/* 111 is old sigsuspend */
				/* 112 is old sigstack */
				/* 113 is old recvmsg */
				/* 114 is old sendmsg */
				/* 115 is obsolete vtrace */
#define	GETTIMEOFDAY_NO	116
#define	GETRUSAGE_NO	117
#define	GETSOCKOPT_NO	118
#define	READV_NO	120
#define	WRITEV_NO	121
#define	SETTIMEOFDAY_NO	122
#define	FCHOWN_NO	123
#define	FCHMOD_NO	124
				/* 125 is old recvfrom */
#define	SETREUID_NO	126
#define	SETREGID_NO	127
#define	RENAME_NO	128
				/* 129 is old truncate */
				/* 130 is old ftruncate */
#define	FLOCK_NO	131
#define	MKFIFO_NO	132
#define	SENDTO_NO	133
#define	SHUTDOWN_NO	134
#define	SOCKETPAIR_NO	135
#define	MKDIR_NO	136
#define	RMDIR_NO	137
#define	UTIMES_NO	138
				/* 139 is obsolete 4.2 sigreturn */
#define	ADJTIME_NO	140
				/* 141 is old getpeername */
				/* 142 is old gethostid */
				/* 143 is old sethostid */
				/* 144 is old getrlimit */
				/* 145 is old setrlimit */
				/* 146 is old killpg */
#define	SETSID_NO	147
#define	QUOTACTL_NO	148
				/* 149 is old quota */
				/* 150 is old getsockname */
#define	NLM_SYSCALL_NO	154
#define	NFSSVC_NO	155
				/* 156 is old getdirentries */
				/* 157 is freebsd4 statfs */
				/* 158 is freebsd4 fstatfs */
#define	LGETFH_NO	160
#define	GETFH_NO	161
				/* 162 is freebsd4 getdomainname */
				/* 163 is freebsd4 setdomainname */
				/* 164 is freebsd4 uname */
#define	SYSARCH_NO	165
#define	RTPRIO_NO	166
#define	SEMSYS_NO	169
#define	MSGSYS_NO	170
#define	SHMSYS_NO	171
				/* 173 is freebsd6 pread */
				/* 174 is freebsd6 pwrite */
#define	SETFIB_NO	175
#define	NTP_ADJTIME_NO	176
#define	SETGID_NO	181
#define	SETEGID_NO	182
#define	SETEUID_NO	183
				/* 184 is obsolete lfs_bmapv */
				/* 185 is obsolete lfs_markv */
				/* 186 is obsolete lfs_segclean */
				/* 187 is obsolete lfs_segwait */
#define	FREEBSD11_STAT_NO	188
#define	FREEBSD11_FSTAT_NO	189
#define	FREEBSD11_LSTAT_NO	190
#define	PATHCONF_NO	191
#define	FPATHCONF_NO	192
#define	GETRLIMIT_NO	194
#define	SETRLIMIT_NO	195
#define	FREEBSD11_GETDIRENTRIES_NO	196
				/* 197 is freebsd6 mmap */
#define	__SYSCALL_NO	198
				/* 199 is freebsd6 lseek */
				/* 200 is freebsd6 truncate */
				/* 201 is freebsd6 ftruncate */
#define	__SYSCTL_NO	202
#define	MLOCK_NO	203
#define	MUNLOCK_NO	204
#define	UNDELETE_NO	205
#define	FUTIMES_NO	206
#define	GETPGID_NO	207
#define	POLL_NO	209
#define	FREEBSD7___SEMCTL_NO	220
#define	SEMGET_NO	221
#define	SEMOP_NO	222
				/* 223 is obsolete semconfig */
#define	FREEBSD7_MSGCTL_NO	224
#define	MSGGET_NO	225
#define	MSGSND_NO	226
#define	MSGRCV_NO	227
#define	SHMAT_NO	228
#define	FREEBSD7_SHMCTL_NO	229
#define	SHMDT_NO	230
#define	SHMGET_NO	231
#define	CLOCK_GETTIME_NO	232
#define	CLOCK_SETTIME_NO	233
#define	CLOCK_GETRES_NO	234
#define	KTIMER_CREATE_NO	235
#define	KTIMER_DELETE_NO	236
#define	KTIMER_SETTIME_NO	237
#define	KTIMER_GETTIME_NO	238
#define	KTIMER_GETOVERRUN_NO	239
#define	NANOSLEEP_NO	240
#define	FFCLOCK_GETCOUNTER_NO	241
#define	FFCLOCK_SETESTIMATE_NO	242
#define	FFCLOCK_GETESTIMATE_NO	243
#define	CLOCK_NANOSLEEP_NO	244
#define	CLOCK_GETCPUCLOCKID2_NO	247
#define	NTP_GETTIME_NO	248
#define	MINHERIT_NO	250
#define	RFORK_NO	251
				/* 252 is obsolete openbsd_poll */
#define	ISSETUGID_NO	253
#define	LCHOWN_NO	254
#define	AIO_READ_NO	255
#define	AIO_WRITE_NO	256
#define	LIO_LISTIO_NO	257
#define	FREEBSD11_GETDENTS_NO	272
#define	LCHMOD_NO	274
				/* 275 is obsolete netbsd_lchown */
#define	LUTIMES_NO	276
				/* 277 is obsolete netbsd_msync */
#define	FREEBSD11_NSTAT_NO	278
#define	FREEBSD11_NFSTAT_NO	279
#define	FREEBSD11_NLSTAT_NO	280
#define	PREADV_NO	289
#define	PWRITEV_NO	290
				/* 297 is freebsd4 fhstatfs */
#define	FHOPEN_NO	298
#define	FREEBSD11_FHSTAT_NO	299
#define	MODNEXT_NO	300
#define	MODSTAT_NO	301
#define	MODFNEXT_NO	302
#define	MODFIND_NO	303
#define	KLDLOAD_NO	304
#define	KLDUNLOAD_NO	305
#define	KLDFIND_NO	306
#define	KLDNEXT_NO	307
#define	KLDSTAT_NO	308
#define	KLDFIRSTMOD_NO	309
#define	GETSID_NO	310
#define	SETRESUID_NO	311
#define	SETRESGID_NO	312
				/* 313 is obsolete signanosleep */
#define	AIO_RETURN_NO	314
#define	AIO_SUSPEND_NO	315
#define	AIO_CANCEL_NO	316
#define	AIO_ERROR_NO	317
				/* 318 is freebsd6 aio_read */
				/* 319 is freebsd6 aio_write */
				/* 320 is freebsd6 lio_listio */
#define	YIELD_NO	321
				/* 322 is obsolete thr_sleep */
				/* 323 is obsolete thr_wakeup */
#define	MLOCKALL_NO	324
#define	MUNLOCKALL_NO	325
#define	__GETCWD_NO	326
#define	SCHED_SETPARAM_NO	327
#define	SCHED_GETPARAM_NO	328
#define	SCHED_SETSCHEDULER_NO	329
#define	SCHED_GETSCHEDULER_NO	330
#define	SCHED_YIELD_NO	331
#define	SCHED_GET_PRIORITY_MAX_NO	332
#define	SCHED_GET_PRIORITY_MIN_NO	333
#define	SCHED_RR_GET_INTERVAL_NO	334
#define	UTRACE_NO	335
				/* 336 is freebsd4 sendfile */
#define	KLDSYM_NO	337
#define	JAIL_NO	338
#define	NNPFS_SYSCALL_NO	339
#define	SIGPROCMASK_NO	340
#define	SIGSUSPEND_NO	341
				/* 342 is freebsd4 sigaction */
#define	SIGPENDING_NO	343
				/* 344 is freebsd4 sigreturn */
#define	SIGTIMEDWAIT_NO	345
#define	SIGWAITINFO_NO	346
#define	__ACL_GET_FILE_NO	347
#define	__ACL_SET_FILE_NO	348
#define	__ACL_GET_FD_NO	349
#define	__ACL_SET_FD_NO	350
#define	__ACL_DELETE_FILE_NO	351
#define	__ACL_DELETE_FD_NO	352
#define	__ACL_ACLCHECK_FILE_NO	353
#define	__ACL_ACLCHECK_FD_NO	354
#define	EXTATTRCTL_NO	355
#define	EXTATTR_SET_FILE_NO	356
#define	EXTATTR_GET_FILE_NO	357
#define	EXTATTR_DELETE_FILE_NO	358
#define	AIO_WAITCOMPLETE_NO	359
#define	GETRESUID_NO	360
#define	GETRESGID_NO	361
#define	KQUEUE_NO	362
#define	FREEBSD11_KEVENT_NO	363
				/* 364 is obsolete __cap_get_proc */
				/* 365 is obsolete __cap_set_proc */
				/* 366 is obsolete __cap_get_fd */
				/* 367 is obsolete __cap_get_file */
				/* 368 is obsolete __cap_set_fd */
				/* 369 is obsolete __cap_set_file */
#define	EXTATTR_SET_FD_NO	371
#define	EXTATTR_GET_FD_NO	372
#define	EXTATTR_DELETE_FD_NO	373
#define	__SETUGID_NO	374
				/* 375 is obsolete nfsclnt */
#define	EACCESS_NO	376
#define	AFS3_SYSCALL_NO	377
#define	NMOUNT_NO	378
				/* 379 is obsolete kse_exit */
				/* 380 is obsolete kse_wakeup */
				/* 381 is obsolete kse_create */
				/* 382 is obsolete kse_thr_interrupt */
				/* 383 is obsolete kse_release */
#define	__MAC_GET_PROC_NO	384
#define	__MAC_SET_PROC_NO	385
#define	__MAC_GET_FD_NO	386
#define	__MAC_GET_FILE_NO	387
#define	__MAC_SET_FD_NO	388
#define	__MAC_SET_FILE_NO	389
#define	KENV_NO	390
#define	LCHFLAGS_NO	391
#define	UUIDGEN_NO	392
#define	SENDFILE_NO	393
#define	MAC_SYSCALL_NO	394
#define	FREEBSD11_GETFSSTAT_NO	395
#define	FREEBSD11_STATFS_NO	396
#define	FREEBSD11_FSTATFS_NO	397
#define	FREEBSD11_FHSTATFS_NO	398
#define	KSEM_CLOSE_NO	400
#define	KSEM_POST_NO	401
#define	KSEM_WAIT_NO	402
#define	KSEM_TRYWAIT_NO	403
#define	KSEM_INIT_NO	404
#define	KSEM_OPEN_NO	405
#define	KSEM_UNLINK_NO	406
#define	KSEM_GETVALUE_NO	407
#define	KSEM_DESTROY_NO	408
#define	__MAC_GET_PID_NO	409
#define	__MAC_GET_LINK_NO	410
#define	__MAC_SET_LINK_NO	411
#define	EXTATTR_SET_LINK_NO	412
#define	EXTATTR_GET_LINK_NO	413
#define	EXTATTR_DELETE_LINK_NO	414
#define	__MAC_EXECVE_NO	415
#define	SIGACTION_NO	416
#define	SIGRETURN_NO	417
#define	GETCONTEXT_NO	421
#define	SETCONTEXT_NO	422
#define	SWAPCONTEXT_NO	423
#define	FREEBSD13_SWAPOFF_NO	424
#define	__ACL_GET_LINK_NO	425
#define	__ACL_SET_LINK_NO	426
#define	__ACL_DELETE_LINK_NO	427
#define	__ACL_ACLCHECK_LINK_NO	428
#define	SIGWAIT_NO	429
#define	THR_CREATE_NO	430
#define	THR_EXIT_NO	431
#define	THR_SELF_NO	432
#define	THR_KILL_NO	433
#define	FREEBSD10__UMTX_LOCK_NO	434
#define	FREEBSD10__UMTX_UNLOCK_NO	435
#define	JAIL_ATTACH_NO	436
#define	EXTATTR_LIST_FD_NO	437
#define	EXTATTR_LIST_FILE_NO	438
#define	EXTATTR_LIST_LINK_NO	439
				/* 440 is obsolete kse_switchin */
#define	KSEM_TIMEDWAIT_NO	441
#define	THR_SUSPEND_NO	442
#define	THR_WAKE_NO	443
#define	KLDUNLOADF_NO	444
#define	AUDIT_NO	445
#define	AUDITON_NO	446
#define	GETAUID_NO	447
#define	SETAUID_NO	448
#define	GETAUDIT_NO	449
#define	SETAUDIT_NO	450
#define	GETAUDIT_ADDR_NO	451
#define	SETAUDIT_ADDR_NO	452
#define	AUDITCTL_NO	453
#define	_UMTX_OP_NO	454
#define	THR_NEW_NO	455
#define	SIGQUEUE_NO	456
#define	KMQ_OPEN_NO	457
#define	KMQ_SETATTR_NO	458
#define	KMQ_TIMEDRECEIVE_NO	459
#define	KMQ_TIMEDSEND_NO	460
#define	KMQ_NOTIFY_NO	461
#define	KMQ_UNLINK_NO	462
#define	ABORT2_NO	463
#define	THR_SET_NAME_NO	464
#define	AIO_FSYNC_NO	465
#define	RTPRIO_THREAD_NO	466
#define	SCTP_PEELOFF_NO	471
#define	SCTP_GENERIC_SENDMSG_NO	472
#define	SCTP_GENERIC_SENDMSG_IOV_NO	473
#define	SCTP_GENERIC_RECVMSG_NO	474
#define	PREAD_NO	475
#define	PWRITE_NO	476
#define	MMAP_NO	477
#define	LSEEK_NO	478
#define	TRUNCATE_NO	479
#define	FTRUNCATE_NO	480
#define	THR_KILL2_NO	481
#define	FREEBSD12_SHM_OPEN_NO	482
#define	SHM_UNLINK_NO	483
#define	CPUSET_NO	484
#define	CPUSET_SETID_NO	485
#define	CPUSET_GETID_NO	486
#define	CPUSET_GETAFFINITY_NO	487
#define	CPUSET_SETAFFINITY_NO	488
#define	FACCESSAT_NO	489
#define	FCHMODAT_NO	490
#define	FCHOWNAT_NO	491
#define	FEXECVE_NO	492
#define	FREEBSD11_FSTATAT_NO	493
#define	FUTIMESAT_NO	494
#define	LINKAT_NO	495
#define	MKDIRAT_NO	496
#define	MKFIFOAT_NO	497
#define	FREEBSD11_MKNODAT_NO	498
#define	OPENAT_NO	499
#define	READLINKAT_NO	500
#define	RENAMEAT_NO	501
#define	SYMLINKAT_NO	502
#define	UNLINKAT_NO	503
#define	POSIX_OPENPT_NO	504
#define	GSSD_SYSCALL_NO	505
#define	JAIL_GET_NO	506
#define	JAIL_SET_NO	507
#define	JAIL_REMOVE_NO	508
#define	FREEBSD12_CLOSEFROM_NO	509
#define	__SEMCTL_NO	510
#define	MSGCTL_NO	511
#define	SHMCTL_NO	512
#define	LPATHCONF_NO	513
				/* 514 is obsolete cap_new */
#define	__CAP_RIGHTS_GET_NO	515
#define	CAP_ENTER_NO	516
#define	CAP_GETMODE_NO	517
#define	PDFORK_NO	518
#define	PDKILL_NO	519
#define	PDGETPID_NO	520
#define	PSELECT_NO	522
#define	GETLOGINCLASS_NO	523
#define	SETLOGINCLASS_NO	524
#define	RCTL_GET_RACCT_NO	525
#define	RCTL_GET_RULES_NO	526
#define	RCTL_GET_LIMITS_NO	527
#define	RCTL_ADD_RULE_NO	528
#define	RCTL_REMOVE_RULE_NO	529
#define	POSIX_FALLOCATE_NO	530
#define	POSIX_FADVISE_NO	531
#define	WAIT6_NO	532
#define	CAP_RIGHTS_LIMIT_NO	533
#define	CAP_IOCTLS_LIMIT_NO	534
#define	CAP_IOCTLS_GET_NO	535
#define	CAP_FCNTLS_LIMIT_NO	536
#define	CAP_FCNTLS_GET_NO	537
#define	BINDAT_NO	538
#define	CONNECTAT_NO	539
#define	CHFLAGSAT_NO	540
#define	ACCEPT4_NO	541
#define	PIPE2_NO	542
#define	AIO_MLOCK_NO	543
#define	PROCCTL_NO	544
#define	PPOLL_NO	545
#define	FUTIMENS_NO	546
#define	UTIMENSAT_NO	547
				/* 548 is obsolete numa_getaffinity */
				/* 549 is obsolete numa_setaffinity */
#define	FDATASYNC_NO	550
#define	FSTAT_NO	551
#define	FSTATAT_NO	552
#define	FHSTAT_NO	553
#define	GETDIRENTRIES_NO	554
#define	STATFS_NO	555
#define	FSTATFS_NO	556
#define	GETFSSTAT_NO	557
#define	FHSTATFS_NO	558
#define	MKNODAT_NO	559
#define	KEVENT_NO	560
#define	CPUSET_GETDOMAIN_NO	561
#define	CPUSET_SETDOMAIN_NO	562
#define	GETRANDOM_NO	563
#define	GETFHAT_NO	564
#define	FHLINK_NO	565
#define	FHLINKAT_NO	566
#define	FHREADLINK_NO	567
#define	FUNLINKAT_NO	568
#define	COPY_FILE_RANGE_NO	569
#define	__SYSCTLBYNAME_NO	570
#define	SHM_OPEN2_NO	571
#define	SHM_RENAME_NO	572
#define	SIGFASTBLOCK_NO	573
#define	__REALPATHAT_NO	574
#define	CLOSE_RANGE_NO	575
#define	RPCTLS_SYSCALL_NO	576
#define	__SPECIALFD_NO	577
#define	AIO_WRITEV_NO	578
#define	AIO_READV_NO	579
#define	SCHED_GETCPU_NO	581
#define	SWAPOFF_NO	582
#define	_NOMAXSYSCALL	583

#endif /* SYSCALLS_BSD */
