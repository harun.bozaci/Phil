#ifndef STRING_VIEW_H
#define STRING_VIEW_H

#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

struct StringView
{
  const char* begin;
  size_t size;
};

typedef struct StringView SV;

#define SV_NPOS ULONG_MAX

#define SVConstruct(ptr) ((SV) { .begin = (ptr), .size = strlen((ptr)) })

#define SVConsSize(ptr, siz) ((SV) {.begin = (ptr), .size = (siz) })

#define SVEqual(lhs, rhs) ((lhs).size == (rhs).size && (!strncmp((lhs).begin, (rhs).begin, (lhs).size)))

#define SVNEqual(lhs,rhs) !(SVEqual(lhs,rhs))

#define SVLShift(sv, offset, sv_err) \
  do \
  { \
    if ((sv).size > (offset)) { \
      (sv).begin += (offset); \
      (sv).size -= (offset); \
      (sv_err) = 0; \
    } else { \
      (sv_err) = 1; \
    }\
  } while(0)

// I am not entirely sure 
// if it is something SV has to do 
// since views are safe readonly lightweight respresentation of string
#define SVDestroy(sv) \
  do \
  { \
    if((0 < (sv).size) && (NULL != (sv).begin)) \
    { \
      free((void*) (sv).begin); \
      (sv).begin = NULL; \
      (sv).size = 0; \
    } \
  } while(0)

void SVPrint(SV sv);

size_t SVFindFirstOf(SV sv, char ch);
size_t SVFindFirstNotOf(SV sv, char ch);
size_t SVFindFirstNoneOf(SV sv, SV group);

#endif

