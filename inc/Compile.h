#ifndef COMPILE_H
#define COMPILE_H

#include "SV.h"

void Compile(const char *fname, SV assemblyContent);

#endif /* COMPILE_H */
