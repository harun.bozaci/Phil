#ifndef LEXER_H
#define LEXER_H
#include "SV.h"

struct Arena;

typedef struct Arena Arena_t;

typedef struct TokenNode TokenNode_t;

typedef TokenNode_t *TokenNodePtr_t;

TokenNodePtr_t Lexer(SV input, Arena_t *token_buffer);

#endif // LEXER_H
