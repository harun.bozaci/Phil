/* STD HEADERS */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>
#include <assert.h>

/* CUSTOM HEADERS */
#include "SV.h"
#include "Arena.h"
#include "Lexer.h"
#include "Token.h"
#include "SyntaxTreeGenerator.h"
#include "AssemblyGenerator.h"
#include "Compile.h"

SV ReadFile(const char* fname, Arena_t *arena)
{
  FILE* input_file = NULL;

  if((input_file = fopen(fname, "rb")) == NULL)
  {
    fprintf(stderr, "file `%s` could not be opened\n", fname);
    exit(EXIT_FAILURE);
  }

  fseek(input_file, 0, SEEK_END);
  size_t file_size =  ftell(input_file) - 1;
  char *file_buf = (char *) arena_alloc(arena, file_size + 1);

  if (file_buf == NULL)
  {
    fprintf(stderr, "buffer for file couldn't be allocated\n");
    exit(EXIT_FAILURE);
  }

  fseek(input_file, 0, SEEK_SET);

  size_t n = 0, m = 0;

  while((n = fread(file_buf + m, 1, file_size, input_file)) < file_size && n != 0) m += n;

  fclose(input_file);
  return SVConsSize(file_buf, file_size);
}


int main(int argc, char *argv[])
{
  if(argc < 3)
  {
    fprintf(stderr, "usage:\n %s <input> <output>\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  const char *input = argv[1];
  const char *output = argv[2];
  Arena_t * file_buffer = init_arena();

  if(file_buffer == NULL)
  {
    fprintf(stderr, "Memory couldn't be allocated for file\n");
    exit(EXIT_FAILURE);
  }

  SV input_view = ReadFile(input, file_buffer);

  Arena_t * token_buffer = init_arena();

  if(token_buffer == NULL)
  {
    fprintf(stderr, "Memory couldn't be allocated for tokens\n");
    exit(EXIT_FAILURE);
  }
  TokenNodePtr_t token_start = Lexer(input_view, token_buffer);

  if(NULL == token_start)
  {
    fprintf(stderr, "Empty lexical analyze output\n");
    exit(EXIT_FAILURE);
  }
  // turn it on to debug
#if 0
  for(TokenNodePtr_t iter = token_start; iter != NULL; iter = iter->next)
  {
    PrintToken(iter);
  }
#endif

  Arena_t* parser_buffer = init_arena();
  ParserPtr_t init = GenerateParseTree(token_start, parser_buffer);

  if(NULL == init)
  {
    fprintf(stderr, "Empty parse output\n");
    exit(EXIT_FAILURE);
  }

#if 0
  for(ParserPtr_t iter = init; iter != NULL; iter = iter->next)
  {
    PrintParser(iter);
  }
#endif
  Arena_t *output_buffer = init_arena(); 
  SV assembly_content = GenerateAssemblyCode(init, output_buffer); 

  Compile(output, assembly_content);

  destroy_arena(output_buffer);
  destroy_arena(parser_buffer);
  destroy_arena(token_buffer);
  destroy_arena(file_buffer); 

  return 0;
}
