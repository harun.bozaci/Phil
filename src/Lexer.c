/* STD HEADERS */
#include <stdio.h>
#include <stdbool.h>
/* CUSTOM HEADERS */
#include "Lexer.h"
#include "Token.h"
#include "Arena.h"
#include "Common.h"
#include "Utility.h"
#include "SV.h"

#define CONSUMED_TOKEN_SIZE(TOKEN_PAIR) \
  ((char*)(TOKEN_PAIR).second->token.value.begin \
   - (char*)(TOKEN_PAIR).first->token.value.begin) \
   + (TOKEN_PAIR).second->token.value.size


bool IsDQuote(SV sv)
{
  return (sv.size == 1 && sv.begin[0] == '"');
}

static TokenNodePtr_t GlueStringLiterals(TokenNodePtr_t start)
{
  TokenNodePtr_t last = NULL;
  for(TokenNodePtr_t iter = start->next; iter != NULL; iter = iter->next)
  {
    if(IsDQuote(iter->token.value))
    {
      TokenNodePtrPair pair = {.first = start, .second = iter };
      start->token.type.category = TOKEN_LITERAL;
      start->token.type.subToken.stt.subLit = TOK_LIT_STRING;
      start->token.value.size = CONSUMED_TOKEN_SIZE(pair);
      last = iter;
      break;
    }
  }

  return last;
}

OperatorTokenType_t EvaluateOperator(SV value)
{
  if(value.size == 1)
  {
    switch(value.begin[0])
    {
      case '=': return TOK_OP_EQ; break;
      case '+': return TOK_OP_PLUS; break;
      case '-': return TOK_OP_DASH; break;
      case '*': return TOK_OP_STAR; break;
      case '/': return TOK_OP_FSLASH; break;
      case '%': return TOK_OP_MODULO; break;
    }
  }
  return TOK_OP_NO;
}

KeywordTokenType_t EvaluateKeyword(SV value)
{
 KeywordTokenType_t idx; 
  for(idx = TOK_KW_FOR; idx < TOK_KW_NO; ++idx)
  {
    if(SVEqual(value, keywords[idx]))
    {
      break;
    }
  }
  return idx;
}

bool EvaluateTokens(TokenNodePtr_t start)
{
  for(TokenNodePtr_t iter = start; iter != NULL; iter = iter->next)
  {
    if(iter->token.type.category == TOKEN_NO)
    {
      if(IsSign(iter->token.value.begin[0]))
      {
        iter->token.type.category = TOKEN_OPERATOR;
        iter->token.type.subToken.stt.subOp = EvaluateOperator(iter->token.value);
        if(iter->token.type.subToken.stt.subOp == TOK_OP_NO)
        {
          fprintf(stderr, "Unknown operator: ");
          SVPrint(iter->token.value);
          exit(EXIT_FAILURE);
        }
      }
      else if(IsKeyword(iter->token.value))
      {
        iter->token.type.category = TOKEN_KEYWORD;
        iter->token.type.subToken.stt.subKw = EvaluateKeyword(iter->token.value);
        if(iter->token.type.subToken.stt.subKw == TOK_KW_NO)
        {
          fprintf(stderr, "Unknown keyword: ");
          SVPrint(iter->token.value);
          exit(EXIT_FAILURE);
        }

      }
      else if(IsLiteral(iter->token.value))
      {
        iter->token.type.category = TOKEN_LITERAL;
        iter->token.type.subToken.stt.subLit = TOK_LIT_INT;
      }
      else if(IsIdentifier(iter->token.value))
      {
        iter->token.type.category = TOKEN_IDENTIFIER;
      }
      else if(IsSemiColon(iter->token.value.begin[0]))
      {
        iter->token.type.category = TOKEN_SEMICOLON;
      }
      else
      {
        fprintf(stderr, "Unknown token `%.*s`\n", (int)iter->token.value.size, iter->token.value.begin);
        return false;
      }
    }
  }
  return true;
}

TokenNodePtr_t Lexer(SV input, Arena_t *token_buffer)
{
  TokenNodePtr_t first = NULL;
  TokenNodePtr_t last = NULL;
  size_t index = 0;
  SV temp = input;

  while(index < input.size)
  {
    size_t offset;
    if((offset = SVFindFirstNoneOf(temp, spaces)) != SV_NPOS)
    {
      index += offset;
      SVLShift(temp, offset, offset);
      if(offset)
      {
        fprintf(stderr, "Can not shift left more than SV boundaries\n");
        exit(EXIT_FAILURE);
      }
    }

    TokenNodePtrPair p = Tokenize(temp, token_buffer);

    if(NULL == first)
    {
      first = p.first;
    }
    else
    {
      last->next = p.first;
    }

    last = p.second;
    index +=  CONSUMED_TOKEN_SIZE(p);
    temp.begin = &input.begin[index];
    temp.size = input.size - index;
  }

  TokenNodePtr_t iter = first;
  while(iter != NULL)
  {
    if(IsDQuote(iter->token.value))
    {
      TokenNodePtr_t upTo = GlueStringLiterals(iter);
      if(!upTo)
      {
        fprintf(stderr, "Not enclosed string literal\n");
      }
      iter->next = upTo->next;
      iter = upTo->next;
      continue;
    }
    iter = iter->next;
  }

  if(!EvaluateTokens(first))
  {
    destroy_arena(token_buffer);
    exit(EXIT_FAILURE);
  }

  return first;
}
