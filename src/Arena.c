#include "Arena.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#if !(MAX_ARENA_CAPACITY >= 1024) || !(MAX_ARENA_CAPACITY <= 2 * 1024 * 1024)
#error "Arena capacity can not be acceptable"
#endif

#if !(MAX_ARENA_SIZE >= 1) || !(MAX_ARENA_SIZE <= 20)
#error "Arena size can not be acceptable"
#endif

struct ArenaMetadata
{
  unsigned char* start;
  size_t capacity;
  int chunk_no;
};

typedef struct ArenaMetadata metadata;

static unsigned char ARENA_MEMORY[(MAX_ARENA_CAPACITY + sizeof(Arena_t) + sizeof(metadata)) * MAX_ARENA_SIZE] = {0};

static int arena_allocated = 0;

static int find_first_free_arena()
{
  for(int i = 0; i < MAX_ARENA_SIZE; ++i)
  {
    if((arena_allocated & (1 << i)) == 0)
    {
      arena_allocated |= 1 << i;
      return i;
    }
  }
  return -1;
}

static int free_arena_slot(int i)
{
  if(i < 0 || i >= MAX_ARENA_SIZE)
  {
    fprintf(stderr, "Unknown arena slot.\n");
    return -1;
  }

  if((arena_allocated & (1 << i)) == 0)
  {
    fprintf(stderr, "Arena slot is already free.\n");
    return -1;
  }

  arena_allocated &= ~(1 << i);
  return 0;
}


Arena_t* init_arena(void)
{
  int empty_slot;
  if((empty_slot = find_first_free_arena()) == -1)
  {
    fprintf(stderr, "No free arena slot could be found!\n");
    return NULL;
  }

  unsigned char *memory_starts = &ARENA_MEMORY[MAX_ARENA_CAPACITY * empty_slot];
  metadata m;
  m.start = &ARENA_MEMORY[MAX_ARENA_CAPACITY * empty_slot] + sizeof(Arena_t) + sizeof(metadata);
  m.capacity = MAX_ARENA_CAPACITY;
  m.chunk_no = empty_slot;

  memcpy(&memory_starts[0], &m, sizeof m);

  Arena_t arena;
  arena.block = (void *)&memory_starts[0];
  arena.size = 0;

  unsigned char *arena_starts = &memory_starts[sizeof m];

  memcpy(&arena_starts[0], &arena, sizeof arena);

  return (Arena_t*) &arena_starts[0];
}

void* arena_alloc(Arena_t* arena, size_t size)
{
  if(NULL == arena)
  {
    fprintf(stderr, "Memory can not be allocated from empty arena\n");
    return NULL;
  }

  if(size == 0)
  {
    fprintf(stderr, "0 size memory can not be allocated from arena\n");
    return NULL;
  }


  metadata *m = (metadata *)arena->block;

  if(NULL == m 
    || NULL == m->start 
    || m->capacity != MAX_ARENA_CAPACITY
    || m->chunk_no < 0
    || m->chunk_no >= MAX_ARENA_SIZE)
  {
    fprintf(stderr, "Arena has not been initialized yet\n");
    return NULL;
  }

  if((unsigned char*)m != &ARENA_MEMORY[MAX_ARENA_CAPACITY * m->chunk_no] 
      || m->start != &ARENA_MEMORY[MAX_ARENA_CAPACITY * m->chunk_no] + sizeof(metadata) + sizeof(Arena_t) )
  {
    fprintf(stderr, "Unknown start address for arena\n");
    return NULL;
  }
  

  if(arena->size + size > m->capacity)
  {
    fprintf(stderr, "No space left to allocate in arena\n");
    return NULL;
  }

  void *allocated_memory = (void *) &m->start[arena->size];
  arena->size += size;

  return allocated_memory;
}

void destroy_arena(Arena_t *arena)
{
  if(NULL == arena)
  {
    fprintf(stderr, "Memory can not be allocated from empty arena\n");
    exit(EXIT_FAILURE);
  }

  metadata *m = (metadata *)arena->block;

  if(NULL == m || NULL == m->start || m->capacity != MAX_ARENA_CAPACITY)
  {
    fprintf(stderr, "Arena has not been initialized yet\n");
    exit(EXIT_FAILURE);
  }

  if(free_arena_slot(m->chunk_no) == -1)
  {
    fprintf(stderr, "Something went wrong while deallocating arena\n");
    exit(EXIT_FAILURE);
  }

  m->start = NULL;
  m->capacity = 0;
  m->chunk_no = - 1;
  arena->block = NULL;
  arena->size = 0;
}
