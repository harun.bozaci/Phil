/* STD HEADERS */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/* CUSTOM HEADERS */
#include "Token.h"
#include "SyntaxTreeGenerator.h"
#include "Arena.h"
#include "AssemblyGenerator.h"
#include "syscall.h"

static int ExitExpr(SV exitCode, char *buf)
{
	static const char* args = "\t\tmov rax, %u\n\t\tmov rdi, %u\n";
	static const char interrupt[] = "\t\tint 0x80\n\t\tret\n";
	int siz = 0;
	if(exitCode.begin == NULL || exitCode.size == 0 || exitCode.size > 3)
	{
		fprintf(stderr, "Exit code not viable, exitCode: ");
		SVPrint(exitCode);
		return siz;
	}

	unsigned short exitValue = 0;
	unsigned int ten = 1;
	for(size_t idx = 0; idx < exitCode.size; ++idx)
	{
		exitValue += (exitCode.begin[exitCode.size - idx - 1] - '0') * ten;
		ten *= 10;
	}

	if(exitValue > 255)
	{
		fprintf(stderr, "Exit code not viable, exceed 255, exitCode: %d", exitValue);
		return siz;
	}

	int numberOfBytes = sprintf(buf, args, EXIT_NO, exitValue);
	siz += numberOfBytes;
	buf = &buf[numberOfBytes];
	memcpy(buf, interrupt, sizeof interrupt - 1);
	siz += sizeof interrupt - 1;
	return siz;
}


SV GenerateAssemblyCode(ParserPtr_t parser, Arena_t *output_buffer) 
{ 
  ParserPtr_t iter = parser; 
  if(NULL == iter) 
  { 
    fprintf(stderr, "Empty parser\n"); 
    exit(EXIT_FAILURE); 
  } 
  SV ret; 
  char* output = arena_alloc(output_buffer, MAX_ARENA_CAPACITY); 
  size_t size = 0; 
  ret.begin = output; 
  static const char entry[] = "BITS 64\n\nglobal _start\n_start:\n"; 
  memcpy(&output[0], &entry[0], sizeof(entry) - 1); 
  size += sizeof(entry) - 1; 
  output = &output[size]; 
  while(iter != NULL) 
  { 
    switch(parser->expression.type) 
    { 
      case EXPR_EXIT:  
	int siz = ExitExpr(parser->expression.operands->token.value, output);
	if(siz == 0)
	{
		fprintf(stderr, "Something occured while generating assembly code\n");
		exit(EXIT_FAILURE);
	}
	size += siz;
	output = &output[siz];
        break; 
      case EXPR_NO: 
        break; 
    } 
    iter = iter->next; 
  } 
  ret.size = size; 
  return ret; 
} 
