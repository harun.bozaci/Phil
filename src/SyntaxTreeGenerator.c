/* STD HEADERS */
#include <stdio.h>
#include <assert.h>
#include <stdbool.h>
/* CUSTOM HEADERS */
#include "SyntaxTreeGenerator.h"
#include "Token.h"
#include "SV.h"
#include "Common.h"
#include "Arena.h"

#define INVALID_TOKEN_PTR (void *)(-1)

struct ParserReturnType {
  TokenNodePtr_t operandStart;
  TokenNodePtr_t operandEnd;
  size_t numberOfOperands;
  ExpressionType_t expr;
  bool needSemicolon;
};

typedef struct ParserReturnType ParserReturnType_t;

#define INVALID_PARSER_RETURN_TYPE \
                (ParserReturnType_t) { \
                                      .operandStart = (INVALID_TOKEN_PTR) \
                                      , .operandEnd = (INVALID_TOKEN_PTR) \
                                      , .numberOfOperands = 0 \
                                      , .expr = EXPR_NO \
                                      , .needSemicolon = false \
                                      }

#define IS_INV_PARSER_RT(PRT) \
  (((INVALID_TOKEN_PTR) == (PRT).operandStart) \
   && ((INVALID_TOKEN_PTR) == (PRT).operandEnd) \
   && (0 == (PRT).numberOfOperands) \
   && (EXPR_NO == (PRT).expr) \
   && (false == (PRT).needSemicolon))
                                      

typedef ParserReturnType_t(*ParserFunc_t)(TokenNodePtr_t);

static ParserReturnType_t ParserFuncLiteralStringLiteral(TokenNodePtr_t);
static ParserReturnType_t ParserFuncLiteralIntegerLiteral(TokenNodePtr_t);

static const ParserFunc_t ParserFuncLiteralFuncs[] = 
                          { ParserFuncLiteralStringLiteral
                            , ParserFuncLiteralIntegerLiteral
                            , NULL };

static ParserReturnType_t ParserFuncIdentifier(TokenNodePtr_t);

static const ParserFunc_t ParserFuncIdentifierFuncs[] =
                          { ParserFuncIdentifier, NULL };

static ParserReturnType_t ParserFuncOperatorEqualOperator(TokenNodePtr_t);
static ParserReturnType_t ParserFuncOperatorPlusOperator(TokenNodePtr_t);
static ParserReturnType_t ParserFuncOperatorDashOperator(TokenNodePtr_t);
static ParserReturnType_t ParserFuncOperatorStarOperator(TokenNodePtr_t);
static ParserReturnType_t ParserFuncOperatorForwardSlashOperator(TokenNodePtr_t);
static ParserReturnType_t ParserFuncOperatorModuloOperator(TokenNodePtr_t);

static const ParserFunc_t ParserFuncOperatorFuncs[] = 
                          { ParserFuncOperatorEqualOperator
                            , ParserFuncOperatorPlusOperator
                            , ParserFuncOperatorDashOperator
                            , ParserFuncOperatorStarOperator
                            , ParserFuncOperatorForwardSlashOperator
                            , ParserFuncOperatorModuloOperator
                            , NULL };

static ParserReturnType_t ParserFuncKeywordForKeyword(TokenNodePtr_t);
static ParserReturnType_t ParserFuncKeywordIfKeyword(TokenNodePtr_t);
static ParserReturnType_t ParserFuncKeywordReturnKeyword(TokenNodePtr_t);
static ParserReturnType_t ParserFuncKeywordInKeyword(TokenNodePtr_t);
static ParserReturnType_t ParserFuncKeywordExitKeyword(TokenNodePtr_t);

static const ParserFunc_t ParserFuncKeywordFuncs[] =
                          { ParserFuncKeywordForKeyword
                            , ParserFuncKeywordIfKeyword
                            , ParserFuncKeywordReturnKeyword
                            , ParserFuncKeywordInKeyword
                            , ParserFuncKeywordExitKeyword
                            , NULL };

static ParserReturnType_t ParserFuncSemicolon(TokenNodePtr_t);

static const ParserFunc_t ParserFuncSemicolonFuncs[] =
                          { ParserFuncSemicolon
                            , NULL };

static const ParserFunc_t *ParserFuncs[TOKEN_NO] =
                          { ParserFuncLiteralFuncs
                            , ParserFuncIdentifierFuncs
                            , ParserFuncOperatorFuncs
                            , ParserFuncKeywordFuncs
                            , ParserFuncSemicolonFuncs
                            };

ParserPtr_t GenerateParseTree(TokenNodePtr_t token_start, Arena_t *parser_buffer)
{
  TokenNodePtr_t iter = token_start;
  ParserPtr_t first = NULL;
  ParserPtr_t last = NULL;
  while(iter != NULL)
  {
    size_t category = (size_t) iter->token.type.category;
    size_t category_index = (size_t) iter->token.type.subToken.idx;
    ParserReturnType_t parserReturn = ParserFuncs[category][category_index](iter);

    if(IS_INV_PARSER_RT(parserReturn))
    {
      fprintf(stderr, "Parsing error, unknown expresison\n");
      exit(EXIT_FAILURE);
    }

    TokenNodePtr_t next = parserReturn.operandEnd;
    if(true == parserReturn.needSemicolon)
    {
      ParserReturnType_t semicolonParserReturn = ParserFuncs[TOKEN_SEMICOLON][0](next);
      if(IS_INV_PARSER_RT(semicolonParserReturn))
      {
        fprintf(stderr, "Syntax error\n");
        exit(EXIT_FAILURE);
      }
      next = semicolonParserReturn.operandEnd; 
    }

    ParserPtr_t temp;
    if(NULL == first)
    {
      temp = first = arena_alloc(parser_buffer, sizeof(Parser_t));
    }
    else
    {
      temp = last->next = arena_alloc(parser_buffer, sizeof(Parser_t));
    }

    temp->expression.type = parserReturn.expr;
    temp->expression.operands = parserReturn.operandStart;
    temp->expression.noOperands = parserReturn.numberOfOperands;
    temp->next = NULL;
    last = temp;
    iter = next;
  }
  return first;
}

void PrintParser(ParserPtr_t node)
{
    printf("Expression Type: ");
    switch(node->expression.type)
    {
      case EXPR_EXIT:
        printf("EXIT, "); break;
      case EXPR_NO:
        printf("Unknown Expression, "); break;
    }
    printf("# of Expression: %lu\nTokens:\n", node->expression.noOperands);

    TokenNodePtr_t t = node->expression.operands;
    for(size_t token_idx = 0; token_idx < node->expression.noOperands; ++token_idx)
    {
      PrintToken(t);
      t = t->next;
    }
}

ParserReturnType_t ParserFuncLiteralStringLiteral(TokenNodePtr_t)
{
  ParserReturnType_t ret = INVALID_PARSER_RETURN_TYPE;
  return ret; 
}

ParserReturnType_t ParserFuncLiteralIntegerLiteral(TokenNodePtr_t)
{
  ParserReturnType_t ret = INVALID_PARSER_RETURN_TYPE;
  return ret; 
}

ParserReturnType_t ParserFuncIdentifier(TokenNodePtr_t)
{
  ParserReturnType_t ret = INVALID_PARSER_RETURN_TYPE;
  return ret; 
}

ParserReturnType_t ParserFuncOperatorEqualOperator(TokenNodePtr_t)
{
  ParserReturnType_t ret = INVALID_PARSER_RETURN_TYPE;
  return ret; 
}

ParserReturnType_t ParserFuncOperatorPlusOperator(TokenNodePtr_t)
{
  ParserReturnType_t ret = INVALID_PARSER_RETURN_TYPE;
  return ret; 
}

ParserReturnType_t ParserFuncOperatorDashOperator(TokenNodePtr_t)
{
  ParserReturnType_t ret = INVALID_PARSER_RETURN_TYPE;
  return ret; 
}

ParserReturnType_t ParserFuncOperatorStarOperator(TokenNodePtr_t)
{
  ParserReturnType_t ret = INVALID_PARSER_RETURN_TYPE;
  return ret; 
}

ParserReturnType_t ParserFuncOperatorForwardSlashOperator(TokenNodePtr_t)
{
  ParserReturnType_t ret = INVALID_PARSER_RETURN_TYPE;
  return ret; 
}

ParserReturnType_t ParserFuncOperatorModuloOperator(TokenNodePtr_t)
{
  ParserReturnType_t ret = INVALID_PARSER_RETURN_TYPE;
  return ret; 
}

ParserReturnType_t ParserFuncKeywordForKeyword(TokenNodePtr_t)
{
  ParserReturnType_t ret = INVALID_PARSER_RETURN_TYPE;
  return ret; 
}

ParserReturnType_t ParserFuncKeywordIfKeyword(TokenNodePtr_t)
{
  ParserReturnType_t ret = INVALID_PARSER_RETURN_TYPE;
  return ret; 
}

ParserReturnType_t ParserFuncKeywordReturnKeyword(TokenNodePtr_t)
{
  ParserReturnType_t ret = INVALID_PARSER_RETURN_TYPE;
  return ret; 
}

ParserReturnType_t ParserFuncKeywordInKeyword(TokenNodePtr_t)
{
  ParserReturnType_t ret = INVALID_PARSER_RETURN_TYPE;
  return ret; 
}

ParserReturnType_t ParserFuncKeywordExitKeyword(TokenNodePtr_t tokPtr)
{
  ParserReturnType_t ret = INVALID_PARSER_RETURN_TYPE;
  if(NULL != tokPtr
      && tokPtr->token.type.category == TOKEN_KEYWORD
      && tokPtr->token.type.subToken.stt.subKw == TOK_KW_EXIT)
  {
      TokenNodePtr_t nextTokPtr = tokPtr->next;
      if(NULL != nextTokPtr
          && nextTokPtr->token.type.category == TOKEN_LITERAL
          && nextTokPtr->token.type.subToken.stt.subLit == TOK_LIT_INT)
      {
        ret.operandStart = nextTokPtr;
        ret.operandEnd = nextTokPtr->next;
        ret.numberOfOperands = 1;
        ret.expr = EXPR_EXIT;
        ret.needSemicolon = true;
      }
  }
  return ret;
}

ParserReturnType_t ParserFuncSemicolon(TokenNodePtr_t tokPtr)
{
  ParserReturnType_t ret = INVALID_PARSER_RETURN_TYPE;

  if(NULL != tokPtr
      && tokPtr->token.type.category == TOKEN_SEMICOLON)
  {
    ret.operandEnd = tokPtr->next;
  }
  return ret;
}
