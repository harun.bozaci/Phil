/* STD HEADERS */
#include <stdio.h>
#include <stddef.h>
/* CUSTOM HEADERS */
#include "Token.h"
#include "Arena.h"
#include "Utility.h"

static SV SplitBySpaces(SV input)
{
  for(size_t index = 0; index < input.size; ++index)
  {
    if(IsSpace(input.begin[index]))
    {
      return SVConsSize(input.begin, index);
    }
  }
  return input;
}


static TokenNodePtrPair SplitBySpecials(SV input, Arena_t* arena)
{
  TokenNode_t* init = NULL;
  TokenNode_t* last = NULL;
  size_t start_index = 0;
  for(size_t index = 0; index < input.size; ++index)
  {
    if(IsSpecial(input.begin[index]))
    {
      TokenNode_t* temp;
      if(NULL == init)
      {
        temp = init = arena_alloc(arena, sizeof(TokenNode_t));
      }
      else
      {
        temp = last->next = arena_alloc(arena, sizeof(TokenNode_t));
      }

      temp->token.type.category = TOKEN_NO;
      temp->token.value.begin = &input.begin[start_index];
      temp->token.value.size = (index - start_index) ? (index - start_index) : 1;
      last = temp;
      if(index != start_index)
      {
        last->next = arena_alloc(arena, sizeof(TokenNode_t));
        temp = last->next;
        temp->token.type.category = TOKEN_NO;
        temp->token.value.begin = &input.begin[index];
        temp->token.value.size = 1;
        last = temp;
      }
      last->next = NULL;
      start_index = index + 1;
    }
    
  }
  if(start_index < input.size)
  {
      TokenNode_t* temp;
      if(NULL == init)
      {
        temp = init = arena_alloc(arena, sizeof(TokenNode_t));
      }
      else
      {
        temp = last->next = arena_alloc(arena, sizeof(TokenNode_t));
      }
      temp->token.type.category = TOKEN_NO;
      temp->token.value.begin = &input.begin[start_index];
      temp->token.value.size = input.size - start_index;
      last = temp;
      last->next = NULL;
  }
  return (TokenNodePtrPair){ init, last };
}


TokenNodePtrPair Tokenize(SV input, Arena_t* arena)
{
  SV split = SplitBySpaces(input);
  return SplitBySpecials(split, arena);
}

static const char * GetLiteralTypeCStr(LiteralTokenType_t sub)
{
  switch(sub)
  {
    case TOK_LIT_INT: return "INTEGER"; break;
    case TOK_LIT_STRING: return "STRING"; break;
    case TOK_LIT_NO: fprintf(stderr, "Unknown literal type"); exit(EXIT_FAILURE); break;
  }
  return NULL;
}

static const char * GetOperatorTypeCStr(OperatorTokenType_t sub)
{
  switch(sub)
  {
    case TOK_OP_EQ: return "EQUAL"; break;
    case TOK_OP_PLUS: return "PLUS"; break;
    case TOK_OP_DASH: return "DASH"; break;
    case TOK_OP_STAR: return "STAR"; break;
    case TOK_OP_FSLASH: return "FORWARD SLASH"; break;
    case TOK_OP_MODULO: return "MODULO"; break;
    case TOK_OP_NO: fprintf(stderr, "Unknown operator type"); exit(EXIT_FAILURE); break;
  }
  return NULL;
}

static const char * GetKeywordTypeCStr(KeywordTokenType_t sub)
{
  return keywords[sub].begin;
}

void PrintToken(TokenNodePtr_t token)
{
    switch(token->token.type.category)
    {
      case TOKEN_LITERAL:
        printf("Token Type: LITERAL, Sub Token type: %s, ", GetLiteralTypeCStr(token->token.type.subToken.stt.subLit)); break;
      case TOKEN_OPERATOR:
        printf("Token Type: OPERATOR, Sub Token type: %s, ", GetOperatorTypeCStr(token->token.type.subToken.stt.subOp)); break;
      case TOKEN_KEYWORD:
        printf("Token Type: KEYWORD, Sub Token type: %s, ", GetKeywordTypeCStr(token->token.type.subToken.stt.subKw)); break;
      case TOKEN_IDENTIFIER:
        printf("Token Type: IDENTIFIER, No Sub Token Type, "); break;
      case TOKEN_SEMICOLON:
        printf("Token Type: SEMICOLON, No Sub Token Type, "); break;
      default:
        printf(", Unknown token category, "); break;
    }
    SVPrint(token->token.value);
}
