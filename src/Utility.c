/* STD HEADERS */

/* CUSTOM HEADERS */
#include "Utility.h"
#include "Common.h"
#include <ctype.h>

bool IsSpace(char ch)
{
  if(SVFindFirstNoneOf(SVConsSize(&ch, 1), spaces) !=  SV_NPOS)
  {
    return false;
  }
  return true;
}

bool IsSpecial(char ch)
{
  if(SVFindFirstNoneOf(SVConsSize(&ch, 1), specials) != SV_NPOS)
  {
    return false;
  }
  return true;
}

bool IsKeyword(SV view)
{
  for(size_t idx = 0; idx < sizeof keywords / sizeof keywords[0]; ++idx)
  {
    if(SVEqual(keywords[idx], view))
    {
      return true;
    }
  }
  return false;
}

bool IsSign(char ch)
{
  switch(ch)
  {
    case '=':
      [[fallthrough]];
    case '+':
      [[fallthrough]];
    case '-':
      [[fallthrough]];
    case '*':
      [[fallthrough]];
    case '/':
      [[fallthrough]];
    case '%':
      return true;
  }
  return false;
}

bool IsAllNumeric(SV view)
{
  for(size_t idx = 0; idx < view.size; ++idx)
  {
    if(!isdigit(view.begin[idx])) return false;
  }
  return true;
}

bool IsStringLiteral(SV view)
{
  if(view.size < 2)
  {
    return false;
  }
  if(view.begin[0] != view.begin[view.size - 1] || view.begin[0] != '"')
  {
    return false;
  }

  return true;
}

bool IsLiteral(SV view)
{
  if(IsAllNumeric(view) || IsStringLiteral(view))
  {
    return true;
  }
  return false;
}

bool IsIdentifier(SV view)
{
  if(view.size == 0) return false;
  if(isdigit(view.begin[0])) return false;
  for(size_t idx = 0; idx < view.size; ++idx)
  {
    if(!isalnum(view.begin[idx])) return false;
  }
  return true;
}

bool IsSemiColon(char ch)
{
  return ch == ';';
}
