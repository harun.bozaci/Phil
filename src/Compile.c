/* STD HEADERS */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <errno.h>
#include <string.h>
#include <stdbool.h>

/* CUSTOM HEADERS */
#include "Compile.h"

static bool Assemble(const char *name)
{
	pid_t pid = 0;

	pid = fork();

	switch(pid)
	{
		case -1:
		{
			fprintf(stderr, "Could not create new process to execute assembler\n");
			exit(EXIT_FAILURE);
		}
		case 0:
		{
			if(execlp("nasm", "nasm", "-felf64", name, "-o", ".a.o", NULL) < 0)
			{
				fprintf(stderr, "Could not execute assembler\n");
				exit(EXIT_FAILURE);
			}
			break;
		}
		default:
		{
			int status;
			if(wait(&status) < 0)
			{
				fprintf(stderr, "Execution failed\n");
				return false;
			}

		}
	}

	return true;
}

static bool Link(const char* name)
{
	pid_t pid = 0;

	pid = fork();

	switch(pid)
	{
		case -1:
		{
			fprintf(stderr, "Could not create new process to execute assembler\n");
			exit(EXIT_FAILURE);
		}
		case 0:
		{
			if(execlp("ld", "ld", ".a.o", "-o", name, NULL) < 0)
			{
				fprintf(stderr, "Could not execute assembler\n");
				exit(EXIT_FAILURE);
			}
			break;
		}
		default:
		{
			int status;
			if(wait(&status) < 0)
			{
				fprintf(stderr, "Execution failed\n");
				return false;
			}

		}
	}

	return true;
}

static bool WriteFile(const char *fname, SV content)
{
	FILE *file = NULL;

	if((file = fopen(fname, "wb")) == NULL)
	{
		fprintf(stderr, "File `%s` could not be opened\n\terror: %s", fname, strerror(errno));
		return false;	
	}

	size_t numWritten = fwrite(content.begin, 1, content.size, file);
	if(numWritten != content.size)
	{
		fprintf(stderr, "Content could not be written in file `%s`\n\terror: %s", fname, strerror(errno));
		return false;
	}

	fclose(file);

	return true;
}

void Compile(const char *fname, SV assemblyContent)
{
	if(!WriteFile(".a.s", assemblyContent))
	{
		fprintf(stderr, "Error while creating assembly output\n");
		exit(EXIT_FAILURE);
	}

	if(!Assemble(".a.s"))
	{
		fprintf(stderr, "Error while assembling output\n");
		exit(EXIT_FAILURE);
	}

	if(!Link(fname))
	{
		fprintf(stderr, "Error while linking output(s)\n");
		exit(EXIT_FAILURE);
	}

}
