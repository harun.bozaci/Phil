#include "SV.h"
#include <stdio.h>
#include <string.h>
#include <stdbool.h>


void SVPrint(SV sv)
{
  printf("%.*s\n", (int) sv.size, sv.begin);
}

size_t SVFindFirstOf(SV sv, char ch)
{

  size_t ret = SV_NPOS;
  if(sv.size == 0 || NULL == sv.begin)
  {
    fprintf(stderr, "Empty string view to be searched\n");
    return ret;
  }
  for(size_t sv_idx = 0; sv_idx != sv.size; ++sv_idx)
  {
    if(sv.begin[sv_idx] == ch)
    {
      ret = sv_idx;
      break;
    }
  }
  return ret;
}

size_t SVFindFirstNotOf(SV sv, char ch)
{
  size_t ret = SV_NPOS;
  if(sv.size == 0 || NULL == sv.begin)
  {
    fprintf(stderr, "Empty string view to be searched\n");
    return ret;
  }
  for(size_t sv_idx = 0; sv_idx != sv.size; ++sv_idx)
  {
    if(sv.begin[sv_idx] != ch)
    {
      ret = sv_idx;
      break;
    }
  }
  return ret;
}


// Maybe this one can be optimized in term of SVFindFirstNotOf
// but it is not important at the moment I guess
// but one of TODO
size_t SVFindFirstNoneOf(SV sv, SV group)
{
  size_t ret = SV_NPOS;

  if(group.size == 0 || NULL == group.begin)
  {
    fprintf(stderr, "Empty group\n");
    return ret;
  }

  if(sv.size == 0 || NULL == sv.begin)
  {
    fprintf(stderr, "Empty string view to be searched\n");
    return ret;
  }

  for(size_t sv_idx = 0; sv_idx < sv.size; ++sv_idx)
  {
    bool hit = 0;
    for(size_t grp_idx = 0; grp_idx < group.size; ++grp_idx)
    {
      if(group.begin[grp_idx] == sv.begin[sv_idx])
      {
        hit = 1;
        break;
      }
    }
    if(hit) continue;
    ret = sv_idx;
    break;
  }

  return ret;
}
