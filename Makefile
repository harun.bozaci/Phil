CC=gcc
CFLAGS= -std=c2x -Wall -Wextra -Werror -Wshadow -pedantic
ADDR_CFLAGS= -fsanitize=address
DBG_CFLAGS= -ggdb -O0
INC= ./inc
SRC= ./src
OBJ= main.o SV.o Arena.o Token.o Lexer.o Utility.o SyntaxTreeGenerator.o AssemblyGenerator.o Compile.o
OUTPUT= phil 

.PHONY: all clean all_address


all: $(OBJ)
	$(CC) $(CFLAGS) $(DBG_CFLAGS) $(OBJ) -o $(OUTPUT) -I$(INC) -lrt

all_address: $(OBJ)
	$(CC) $(CFLAGS) $(ADDR_CFLAGS) $(OBJ) -o $(OUTPUT) -I$(INC)

%.o: $(SRC)/%.c
	$(CC) $(CFLAGS) $(DBG_CFLAGS) -o $@ -c $(<) -I$(INC)

clean:
	rm -rf $(OBJ) $(OUTPUT)
